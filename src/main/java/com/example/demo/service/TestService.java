package com.example.demo.service;

import com.example.demo.dao.TestDao;
import com.example.demo.model.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    private final TestDao testDao;

    @Autowired
    public TestService(TestDao testDao) {
        this.testDao = testDao;
    }

    public Object addTest(Test t){
        return testDao.addTest(t);
    }
}
