package com.example.demo.controller;

import com.example.demo.model.Person;
import com.example.demo.model.Test;
import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("person")
@RestController
public class TestController {

    @Autowired
    TestService testService;

    @PostMapping("test")
    public Object addTest(@RequestBody Test t) {
        return this.testService.addTest(t);
    }

    @GetMapping(path = "find/{id}")
    public Person findById(@PathVariable("id") Long id) {
        Person temp = new Person();
        temp.setId(1L);
        return temp;
    }
}
